
# LPI DevOps Tools Engineer Exam Study Resources

The links provided here are from the LPI blog for studying [Exam 701: DevOps Tools Engineer](https://www.lpi.org/our-certifications/exam-701-objectives). When you have finished studying a topic, you can check the checkbox next to it. These resources can also be found at [LPI DevOps study resources](https://github.com/pyguy/lpi-devops-study). This site is an extension of the excellent work of the original contributor with latest information as of  February 2022. I've also included some class notes that you can use for further study. By clicking on this icon, you can get to them. ![alt handout-link](img/handout-20.png).

[I](https://www.linkedin.com/in/chinthakadeshapriya/) wish you  Good luck on your [LPI DevOps Tools Engineer Exam](https://www.lpi.org/our-certifications/exam-701-objectives).

# List of Study Resources

* [Getting Started](Links.md#GettingStarted)
* [Modern Software Development](Links.md#ModernSoftwareDevelopment)
* [Cloud Components and Platforms](Links.md#CloudComponentsandPlatforms)
* [Source Code Management](Links.md#SourceCodeManagement)
* [Continuous Delivery](Links.md#ContinuousDelivery)
* [Container Basics](Links.md#ContainerBasics)
* [Container Orchestration](Links.md#ContainerOrchestration)
* [Container Infrastructure](Links.md#ContainerInfrastructure)
* [Machine Deployment](Links.md#MachineDeployment)
* [Ansible](Links.md#Ansible)
* [Other Configuration Management Tools](Links.md#OtherConfigurationManagementTools)
* [IT Operations and Monitoring](Links.md#ITOperationsandMonitoring)
* [Log Management and Analysis](Links.md#LogManagementandAnalysis)
* [Taking the Test](Links.md#TakingtheTest)
* [Books](Books.md)

