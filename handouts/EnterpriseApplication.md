## Enterprise Application

Enterprise applications typically contain **a large amount of persistent data**, which is typically managed by a database management system. Usually, this database is relational, but NoSQL alternatives are becoming more common. This data is typically more durable and valuable than the applications that process it.

This **data is accessed and manipulated simultaneously time**. The numbers vary greatly; in-house applications may have a few tens of thousands of users, whereas customer-facing web applications may have tens of thousands.

With so much data, enterprise applications require a large number of user interface screens to manage it.

**Integration with other enterprise applications** is required. These systems are built by a variety of teams, some from vendors who sell to a large number of customers, and others built internally specifically for your organization.

Even when different applications access the same data and have significant **conceptual dissonance**, a customer may mean something very different to the sales organization than it does to technical support. 

If you want to change business rules, you'll need sixty to seventy meetings and three vice presidents retiring. Repeat this a thousand times, and you'll have the **complex business "logic"** that lies at the heart of many enterprise applications.
